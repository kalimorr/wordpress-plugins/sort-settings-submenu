(function($){
	$(document).ready(function() {
		var $settingsNav = $('#adminmenu #menu-settings .wp-submenu'),
			$items       = $settingsNav.find('>li'),
			toFilters    = {},
			excludeItems = [
				'options-general.php',
				'options-writing.php',
				'options-reading.php',
				'options-discussion.php',
				'options-general.php?page=media',
				'options-general.php?page=media-library',
				'options-general.php?page=media-taxonomies',
				'options-general.php?page=mime-types',
				'options-permalink.php',
				'options-privacy.php'
			];

		/* Go throw the items and if the target is not in the excluded array, we add it to the filter array */
		$items.each(function (index) {
			var $link = $(this).find('a');
			if ($.inArray($link.attr('href'), excludeItems) === -1 && !$(this).hasClass('wp-submenu-head')) {
				toFilters[$link.text()] = $(this);

				/* Remove the entry from the menu (for a moment) */
				$items.eq(index).remove();
			}
		});

		/* Add a separator between built-in entries and plugins entries */
		$settingsNav.append('<li style="border-top:1px solid #736f6f; margin: 10px 0;"></li>');

		/* Sort the plugins entries and add them back in the menu */
		Object.keys(toFilters).sort().forEach(function (key) {
			$settingsNav.append(toFilters[key])
		});
	});
})(jQuery);