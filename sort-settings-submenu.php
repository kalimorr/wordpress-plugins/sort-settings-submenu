<?php
/**
 * Plugin Name: Sort Settings Submenu
 * Description: Rearrange the "Settings" submenu by alphabetic order to find a plugin setting page more easily.
 * Version: 1.0.0
 * Author: Kalimorr
 * Author URI: https://gitlab.com/kalimorr
 * License: GPLv3 or later
 */

namespace KrrSortSettingsSubmenu;

/**
 * Class SortSettingsSubmenu
 *
 * @package KrrSortSettingsSubmenu
 */
class SortSettingsSubmenu
{

	/**
	 * SortSettingsSubmenu constructor.
	 */
	public function __construct()
	{
		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 51);
	}

	/**
	 *  Styles and scripts
	 */
	public function enqueueScripts()
	{
		wp_enqueue_script(
			'krr-sort-settings-submenu',
			plugin_dir_url(__FILE__) . 'assets/script.js',
			['jquery-core'],
			false,
			true
		);
	}
}

new SortSettingsSubmenu();